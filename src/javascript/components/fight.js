import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    // fighter state, position 0 - attack, 1 - block
    let stateFirstFighter = [false, false];
    let stateSecondFighter = [false, false];
    // super attack state for 1st and 2nd fighters
    let superStatePlayerOne = [false, false, false];
    let superStatePlayerTwo = [false, false, false];
    // initial health of fighters
    let initialHealthFirstPlayer = firstFighter.health;
    let initialHealthSecondPlayer = secondFighter.health;
    // block attack when defending
    let blockAttackOne = [false, false];
    let blockAttackTwo = [false, false];

    function stateHandlerDownOne(e) {
      switch (e.code) {
        case 'KeyQ':
          superStatePlayerOne[0] = true;
          break;
        case 'KeyW':
          superStatePlayerOne[1] = true;
          break;
        case 'KeyE':
          superStatePlayerOne[2] = true;
          break;
      }
    }

    function stateHandlerDownTwo(e) {
      switch (e.code) {
      case 'KeyU':
        superStatePlayerTwo[0] = true;
        break;
      case 'KeyI':
        superStatePlayerTwo[1] = true;
        break;
      case 'KeyO':
        superStatePlayerTwo[2] = true;
        break;
      }
    }
    
    document.addEventListener('keydown', stateHandlerDownOne);
    document.addEventListener('keydown', stateHandlerDownTwo);

    function stateHandlerUpOne(e) {
      switch (e.code) {
        case 'KeyQ':
          superStatePlayerOne[0] = false;
          break;
        case 'KeyW':
          superStatePlayerOne[1] = false;
          break;
        case 'KeyE':
          superStatePlayerOne[2] = false;
          break;
      }
    }

    function stateHandlerUpTwo(e) {
      switch (e.code) {
        case 'KeyU':
          superStatePlayerTwo[0] = false;
          break;
        case 'KeyI':
          superStatePlayerTwo[1] = false;
          break;
        case 'KeyO':
          superStatePlayerTwo[2] = false;
          break;
      }
    }

    document.addEventListener('keyup', stateHandlerUpOne);
    document.addEventListener('keyup', stateHandlerUpTwo);

    function handleSuperAttackOne() {
      // super attack 1st
      if (superStatePlayerOne.every(el => el == true)) {
        secondFighter.health -= 2 * firstFighter.attack;
        let reduceBarValue = (1 - (initialHealthSecondPlayer - secondFighter.health)/initialHealthSecondPlayer) * 100;
        document.getElementById('right-fighter-indicator').style.width = `${reduceBarValue}%`;
        document.removeEventListener('keydown', handleSuperAttackOne);
        setTimeout(() => {document.addEventListener('keydown', handleSuperAttackOne);}, 10000);
      }
    }

    function handleSuperAttackTwo() {
      // super atack 2nd
      if (superStatePlayerTwo.every(el => el == true)) {
        firstFighter.health -= 2 * secondFighter.attack;
        let reduceBarValue = (1 - (initialHealthFirstPlayer - firstFighter.health)/initialHealthFirstPlayer) * 100;
        document.getElementById('left-fighter-indicator').style.width = `${reduceBarValue}%`;
        document.removeEventListener('keydown', handleSuperAttackTwo);
        setTimeout(() => {document.addEventListener('keydown', handleSuperAttackTwo);}, 10000);
      }
    }

    document.addEventListener('keydown', handleSuperAttackOne);
    document.addEventListener('keydown', handleSuperAttackTwo);

    // block attack when block on
    function blockAttackDownOne(e) {
      switch (e.code) {
        case 'KeyA':
          blockAttackOne[0] = true;
          break;
        case 'KeyD':
          blockAttackOne[1] = true;
          break;
      }
    }

    function blockAttackDownTwo(e) {
      switch (e.code) {
        case 'KeyJ':
          blockAttackTwo[0] = true;
          break;
        case 'KeyL':
          blockAttackTwo[1] = true;
          break;
      }
    }

    document.addEventListener('keydown', blockAttackDownOne);
    document.addEventListener('keydown', blockAttackDownTwo);

    function blockAttackUpOne(e) {
      switch (e.code) {
        case 'KeyA':
          blockAttackOne[0] = false;
          break;
        case 'KeyD':
          blockAttackOne[1] = false;
          break;
      }
    }

    function blockAttackUpTwo(e) {
      switch (e.code) {
        case 'KeyJ':
          blockAttackTwo[0] = false;
          break;
        case 'KeyL':
          blockAttackTwo[1] = false;
          break;
      }
    }

    document.addEventListener('keyup', blockAttackUpOne);
    document.addEventListener('keyup', blockAttackUpTwo);

    // main handler for fight

    function handler(e) {
      // 1st Player set block
      if (e.code == controls.PlayerOneBlock) {
        stateFirstFighter[0] = false;
        stateFirstFighter[1] = true;
      }
      // 2nd Player set block
      if (e.code == controls.PlayerTwoBlock) {
        stateSecondFighter[0] = false;
        stateSecondFighter[1] = true;
      }
      // 1st Player set attack
      if (e.code == controls.PlayerOneAttack) {
        stateFirstFighter[0] = true;
        stateFirstFighter[1] = false;
        if (stateSecondFighter[1] == false && !blockAttackOne.every(el => el == true)) {
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          let reduceBarValue = (1 - (initialHealthSecondPlayer - secondFighter.health)/initialHealthSecondPlayer) * 100;
          document.getElementById('right-fighter-indicator').style.width = `${reduceBarValue}%`;
        }
      }
      // 2nd Player set attack
      if (e.code == controls.PlayerTwoAttack) {
        stateSecondFighter[0] = true;
        stateSecondFighter[1] = false;
        if (stateFirstFighter[1] == false && !blockAttackTwo.every(el => el == true)) {
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          let reduceBarValue = (1 - (initialHealthFirstPlayer - firstFighter.health)/initialHealthFirstPlayer) * 100;
          document.getElementById('left-fighter-indicator').style.width = `${reduceBarValue}%`;
        }
      }
      // evaluate fighter's health and resolve winner
      if (firstFighter.health <= 0) {
        document.getElementById('left-fighter-indicator').style.width = '0%';
        document.removeEventListener('keyup', stateHandlerUpOne);
        document.removeEventListener('keyup', stateHandlerUpTwo);
        document.removeEventListener('keydown', stateHandlerDownOne);
        document.removeEventListener('keydown', stateHandlerDownTwo);
        document.removeEventListener('keydown', handleSuperAttackOne);
        document.removeEventListener('keydown', handleSuperAttackTwo);
        document.removeEventListener('keydown', blockAttackDownOne);
        document.removeEventListener('keydown', blockAttackDownTwo);
        document.removeEventListener('keyup', blockAttackUpOne);
        document.removeEventListener('keyup', blockAttackUpTwo);
        document.removeEventListener('keydown', handler);
        resolve(secondFighter);
      }

      if (secondFighter.health <= 0) {
        document.getElementById('right-fighter-indicator').style.width = '0%';
        document.removeEventListener('keyup', stateHandlerUpOne);
        document.removeEventListener('keyup', stateHandlerUpTwo);
        document.removeEventListener('keydown', stateHandlerDownOne);
        document.removeEventListener('keydown', stateHandlerDownTwo);
        document.removeEventListener('keydown', handleSuperAttackOne);
        document.removeEventListener('keydown', handleSuperAttackTwo);
        document.removeEventListener('keydown', blockAttackDownOne);
        document.removeEventListener('keydown', blockAttackDownTwo);
        document.removeEventListener('keyup', blockAttackUpOne);
        document.removeEventListener('keyup', blockAttackUpTwo);
        document.removeEventListener('keydown', handler);
        resolve(firstFighter);
      }
    }

    document.addEventListener('keydown', handler);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  if (damage > 0) {
    return damage;
  } else {
    return 0;
  }
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const power = fighter.defense * dodgeChance;

  return power;
}
