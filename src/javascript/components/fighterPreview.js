import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)**************************************
  if (fighter) {
    const description = createDescription(fighter);
    const imgElement = createFighterImage(fighter);
    fighterElement.append(imgElement, description);
  }

  function createDescription(fighter) {
    const { name, health, attack, defense } = fighter;
    const descElement = createElement({
      tagName: 'div',
      className: 'arena___fighter-name',
    });
    descElement.innerHTML = `name: ${name} health: ${health} attack: ${attack} defense: ${defense}`;

    return descElement;
  }
  // *****************************************************************************************
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
