import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function
  const title = fighter.name;
  const src = fighter.source;
  const bodyElement = document.createElement('img');
  bodyElement.setAttribute('src', src);
  bodyElement.setAttribute('title', title);
  bodyElement.setAttribute('alt', title);

  showModal({ title, bodyElement});
}